# vim: ts=2 sw=2
# - Try to find the required ffmpeg components(default: AVFORMAT, AVUTIL, AVCODEC)
#
# Once done this will define
#  FFMPEG_FOUND         - System has the all required components.
#  FFMPEG_INCLUDE_DIRS  - Include directory necessary for using the required components headers.
#  FFMPEG_LIBRARIES     - Link these to use the required ffmpeg components.
#
# For each of the components it will additionally set.
#   - AVCODEC
#   - AVDEVICE
#   - AVFORMAT
#   - AVUTIL
#   - POSTPROCESS
#   - SWSCALE
#   - SWRESAMPLE
# the following variables will be defined
#  <component>_FOUND          - System has <component>
#  <component>_INCLUDE_DIRS   - Include directory necessary for using the <component> headers
#  <component>_LIBRARIES      - Link these to use <component>
#  <component>_VERSION_STRING - The component's version
#
# Copyright (c) 2006, Matthias Kretz, <kretz@kde.org>
# Copyright (c) 2008, Alexander Neundorf, <neundorf@kde.org>
# Copyright (c) 2011, Michael Jansen, <kde@michael-jansen.biz>
# Copyright (c) 2013, Stephen Baker
# Copyright (c) 2015, Alexander Bessman
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

include(FindPackageHandleStandardArgs)
include(${CMAKE_CURRENT_LIST_DIR}/CMakeFFmpegLibavMacros.cmake)

set(REQUIRED_FFMPEG_VERSION 3.1)
set(_avcodec_ver "57.48.101")
set(_avfilter_ver "6.47.100")
set(_avformat_ver "57.41.100")
set(_avutil_ver "55.28.100")
set(_swscale_ver "4.1.100")
set(_swresample_ver "2.1.100")
set(_postproc_ver "54.0.100")

# The default components were taken from a survey over other FindFFMPEG.cmake files
if (NOT FFmpeg_FIND_COMPONENTS)
  set(FFmpeg_FIND_COMPONENTS AVCODEC AVFORMAT AVUTIL)
endif ()

# Check for cached results. If there are skip the costly part.
if (NOT FFMPEG_LIBRARIES)

  # Check for all possible component.
  find_component(AVCODEC  avcodec  libavcodec/avcodec.h   libavcodec/version.h ${_avcodec_ver})
  find_component(AVFORMAT avformat libavformat/avformat.h libavformat/version.h ${_avformat_ver})
  # find_component(AVDEVICE avdevice libavdevice/avdevice.h libavdevice/version.h)
  find_component(AVUTIL   avutil   libavutil/avutil.h     libavutil/version.h ${_avutil_ver})
  find_component(SWSCALE  swscale  libswscale/swscale.h   libswscale/version.h ${_swscale_ver})
  find_component(POSTPROC postproc libpostproc/postprocess.h libpostproc/version.h ${_postproc_ver})
  find_component(SWRESAMPLE swresample libswresample/swresample.h libswresample/version.h ${_swresample_ver})

  # Check if the required components were found and add their stuff to the FFMPEG_* vars.
  foreach (_component ${FFmpeg_FIND_COMPONENTS})
    if (${_component}_FOUND)
      # message(STATUS "Required component ${_component} present.")
      set(FFMPEG_LIBRARIES   ${FFMPEG_LIBRARIES}   ${${_component}_LIBRARIES})
      list(APPEND FFMPEG_INCLUDE_DIRS ${${_component}_INCLUDE_DIRS})
    else ()
      # message(STATUS "Required component ${_component} missing.")
    endif ()
  endforeach ()

  # Build the include path with duplicates removed.
  if (FFMPEG_INCLUDE_DIRS)
    list(REMOVE_DUPLICATES FFMPEG_INCLUDE_DIRS)
  endif ()

  # cache the vars.
  set(FFMPEG_INCLUDE_DIRS ${FFMPEG_INCLUDE_DIRS} CACHE STRING "The FFmpeg include directories." FORCE)
  set(FFMPEG_LIBRARIES    ${FFMPEG_LIBRARIES}    CACHE STRING "The FFmpeg libraries." FORCE)

  mark_as_advanced(FFMPEG_INCLUDE_DIRS
                   FFMPEG_LIBRARIES)

endif ()

# Now set the noncached _FOUND vars for the components.
foreach (_component AVCODEC AVDEVICE AVFORMAT AVUTIL POSTPROCESS SWSCALE SWRESAMPLE)
  set_component_found(${_component})
endforeach ()

# Compile the list of required vars
set(_FFmpeg_REQUIRED_VARS FFMPEG_LIBRARIES FFMPEG_INCLUDE_DIRS)
foreach (_component ${FFmpeg_FIND_COMPONENTS})
  list(APPEND _FFmpeg_REQUIRED_VARS ${_component}_LIBRARIES ${_component}_INCLUDE_DIRS)
endforeach ()

# Give a nice error message if some of the required vars are missing.
find_package_handle_standard_args(FFMPEG 
                                    REQUIRED_VARS ${_FFmpeg_REQUIRED_VARS}
                                    FAIL_MESSAGE "FFmpeg ${REQUIRED_FFMPEG_VERSION} not found")

# Try build ffmpeg as external project
# Stolen from XBMC Project
if(NOT FFMPEG_FOUND)
    include(ExternalProject)
    file(STRINGS ${CMAKE_SOURCE_DIR}/Externals/depends/ffmpeg/FFMPEG-VERSION VER)
    string(REGEX MATCH "VERSION=[^ ]*$.*" FFMPEG_VER "${VER}")
    list(GET FFMPEG_VER 0 FFMPEG_VER)
    string(SUBSTRING "${FFMPEG_VER}" 8 -1 FFMPEG_VER)
    string(REGEX MATCH "BASE_URL=([^ ]*)" FFMPEG_BASE_URL "${VER}")
    list(GET FFMPEG_BASE_URL 0 FFMPEG_BASE_URL)
    string(SUBSTRING "${FFMPEG_BASE_URL}" 9 -1 FFMPEG_BASE_URL)
    
    # allow user to override the download URL with a local tarball
    # needed for offline build envs
    if(FFMPEG_URL)
       get_filename_component(FFMPEG_URL "${FFMPEG_URL}" ABSOLUTE)
    else()
        set(FFMPEG_URL ${FFMPEG_BASE_URL}/${FFMPEG_VER}.tar.gz)
    endif()
    
    message(STATUS "FFMPEG_URL: ${FFMPEG_URL}")
    message(STATUS "CPU: ${CPU}, ARCH: ${ARCH}, OS:${OS}")
    
    if(CMAKE_CROSSCOMPILING)
        set(CROSS_ARGS -DDEPENDS_PATH=${DEPENDS_PATH}
                       -DPKG_CONFIG_EXECUTABLE=${PKG_CONFIG_EXECUTABLE}
                       -DCROSSCOMPILING=${CMAKE_CROSSCOMPILING}
                       -DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN_FILE}
                       -DOS=${OS}
                       -DARCH=${ARCH}
                       -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
                       -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
                       -DCMAKE_AR=${CMAKE_AR})
    endif()

    externalproject_add(ffmpeg_project
                      URL ${FFMPEG_URL}
                      DOWNLOAD_NAME ffmpeg-${FFMPEG_VER}.tar.gz
                      DOWNLOAD_DIR ${EXECUTABLE_OUTPUT_PATH}/download
                      PREFIX ${EXECUTABLE_OUTPUT_PATH}/ffmpeg
                      CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXECUTABLE_OUTPUT_PATH}
                                 -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                                 -DCORE_SYSTEM_NAME=${CORE_SYSTEM_NAME}
                                 -DCPU=${CPU}
                                 -DENABLE_NEON=${ENABLE_NEON}
                                 -DCMAKE_C_FLAGS=${CMAKE_C_FLAGS}
                                 -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
                                 -DCMAKE_EXE_LINKER_FLAGS=${CMAKE_EXE_LINKER_FLAGS}
                                 -DBUILDER_NAME="carnine"
                                 -DFFMPEG_VER=${FFMPEG_VER}
                                 ${CROSS_ARGS}
                       PATCH_COMMAND ${CMAKE_COMMAND} -E copy
                                     ${CMAKE_SOURCE_DIR}/Externals/depends/ffmpeg/CMakeLists.txt
                                     <SOURCE_DIR> &&
                                    ${CMAKE_COMMAND} -E copy
                                    ${CMAKE_SOURCE_DIR}/Externals/depends/ffmpeg/FindGnuTls.cmake
                                    <SOURCE_DIR>)
    file(WRITE ${EXECUTABLE_OUTPUT_PATH}/ffmpeg/ffmpeg-link-wrapper
"#!/bin/bash
if [[ $@ == *${APP_NAME_LC}.bin* || $@ == *${APP_NAME_LC}.so* || $@ == *${APP_NAME_LC}-test* ]]
then
  avformat=\"$(PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libavformat)\"
  avcodec=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libavcodec`
  avfilter=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libavfilter`
  avutil=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libavutil`
  swscale=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libswscale`
  swresample=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig ${PKG_CONFIG_EXECUTABLE} --libs --static libswresample`
  gnutls=`PKG_CONFIG_PATH=${EXECUTABLE_OUTPUT_PATH}/lib/pkgconfig/ ${PKG_CONFIG_EXECUTABLE}  --libs-only-l --static --silence-errors gnutls`
  $@ $avformat $avcodec $avfilter $swscale $swresample -lpostproc $gnutls
else
  $@
fi")
    file(COPY ${EXECUTABLE_OUTPUT_PATH}/ffmpeg/ffmpeg-link-wrapper
         DESTINATION ${EXECUTABLE_OUTPUT_PATH}
         FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE)
    set(FFMPEG_LINK_EXECUTABLE "${EXECUTABLE_OUTPUT_PATH}/ffmpeg-link-wrapper <CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS> -o <TARGET> <LINK_LIBRARIES>") 
    include_directories(${EXECUTABLE_OUTPUT_PATH}/include)
    set(FFMPEG_FOUND TRUE)
    set_target_properties(ffmpeg_project PROPERTIES FOLDER "External Projects")
    set(FFMPEG_LIBRARIES -lavformat)
endif()

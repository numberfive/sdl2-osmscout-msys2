# This is an Application to Create an Mapview in SDL2 with libosmscout

Sorry for my bad English !

This is an Test Project for the Big Idea of the CarPC.
I will Test Functions and Speed
The Project is Build with Codelite and MSYS2 on Windows 7 64Bit but as 32 Bit Version because Raspberry is also 32 Bit.

The run CMake Command in IDE do not work. Please Use Custom Target CMake or Commandline to Build.

the Env Var
SDL2DIR=D:/Mine/OpenSource/SDL2-2.0.6-mingw/i686-w64-mingw32/
OSMSCOUT_PATH=D:/Mine/OpenSource/libosmscout-code/build/MinGW/Release
Is use to find SDL and OSMScout with Cmake in msys and Windows Custom install path
wenn use Linux Install path you don't need it.

## Build LibOsmscount 
### Mingw on Windows
cmake -E env CXXFLAGS="-O2" cmake .. -G"MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/F/Mine/OpenSource/libosmscout-code/build/MinGW/Release -DOSMSCOUT_ENABLE_SSE=ON
cmake -E env CXXFLAGS="-O2" cmake --build . --target install --config Release
cmake --build . --target install --config Debug

### VS2017 on Windows with vcpkg

Build Depens https://github.com/Microsoft/vcpkg

vcpkg install cairo:x64-windows
vcpkg install sdl2:x64-windows
vcpkg install sdl2-image[libjpeg-turbo]:x64-windows
vcpkg install sdl2-image[libwebp]:x64-windows --recurse
vcpkg install sdl2-mixer:x64-windows
vcpkg install sdl2-net:x64-windows
vcpkg install sdl2-ttf:x64-windows
vcpkg install pango:x64-windows
vcpkg install libxml2:x64-windows
vcpkg install protobuf:x64-windows
vcpkg install zlib:x64-windows
vcpkg upgrade --no-dry-run
vcpkg remove --outdated

Build LibOsmScount http://libosmscout.sourceforge.net/documentation/source/

set QTDIR=F:/Tools/Qt/5.11.1/msvc2017_64

SET MARISA_ROOT=.\marisa
set CMAKE_INCLUDE_PATH=%MARISA_ROOT%\include
set CMAKE_LIBRARY_PATH=%MARISA_ROOT%\lib

cmake -G "Visual Studio 15 2017 Win64" -DQTDIR=%QTDIR% -DCMAKE_PREFIX_PATH=%QTDIR%/lib/cmake -DCMAKE_INSTALL_PREFIX=.\output -DCMAKE_TOOLCHAIN_FILE=F:/Mine/OpenSource/vcpkg/scripts/buildsystems/vcpkg.cmake ..
cmake --build . --target install --config Debug
cmake --build . --target install --config Release


## Auto Build an Test
msys2-launcher-git
https://github.com/msys2/msys2/wiki/Launchers
https://stackoverflow.com/questions/42505300/how-to-setup-jenkins-windows-slave-node-to-support-msys2-mingw-shell

## Day Night Toogle
void DBThread::ToggleDaylight()
{
  {
    QWriteLocker locker(&lock);

    if (!isInitializedInternal()) {
        return;
    }
    qDebug() << "Toggling daylight from " << daylight << " to " << !daylight << "...";
    daylight=!daylight;
    stylesheetFlags["daylight"] = daylight;
  }

  ReloadStyle();

  qDebug() << "Toggling daylight done.";
}

/ new map style may require more data types. when tile is marked as "completed"
  // such data types are never loaded into these tiles
  // so we mark them as "incomplete" to make sure that all types for new stylesheet are loaded
  mapService->InvalidateTileCache();
  osmscout::StyleConfigRef newStyleConfig=std::make_shared<osmscout::StyleConfig>(typeConfig);

  for (const auto& flag : stylesheetFlags) {
    newStyleConfig->AddFlag(flag.first,flag.second);
  }
  

## Karte bewegen
SDL ->
x0                  x100
y0


y100

karte nach oben heißt Bitamp nach unten aber karten für nach oben laden


&& (dx > 5 || dy > 5)

SDL_Texture *texture = SDL_CreateTexture(renderer,
    SDL_PIXELFORMAT_ARGB8888,
    SDL_TEXTUREACCESS_STREAMING,
    width, height);

void *pixels;
int pitch;
SDL_LockTexture(texture, NULL, &pixels, &pitch);
cairo_surface_t *cairo_surface = cairo_image_surface_create_for_data(
    pixels,
    CAIRO_FORMAT_ARGB32,
    width, height, pitch);

paint(cairo_create(cairo_surface));

SDL_UnlockTexture(texture);


https://www.cairographics.org/threaded_animation_with_cairo/
https://github.com/cubicool/cairo-gl-sdl2
http://wiki.libsdl.org/moin.cgi/SDL_GL_BindTexture

### Switch to QT-Frame work ?
https://github.com/riscv/riscv-poky/blob/master/scripts/sysroot-relativelinks.py
https://github.com/JulNadeauCA/libagar

wget https://raw.githubusercontent.com/riscv/riscv-poky/master/scripts/sysroot-relativelinks.py
chmod +x sysroot-relativelinks.py
./sysroot-relativelinks.py sysroot

https://wiki.qt.io/RaspberryPi2EGLFS
http://doc.qt.io/qt-5/embedded-linux.html
http://doc.qt.io/QtForDeviceCreation/qtee-custom-embedded-linux-image.html
http://doc.qt.io/qt-5/qtvirtualkeyboard-basic-example.html
https://visualgdb.com/tutorials/raspberry/qt/embedded/

### Text to Speak
https://github.com/espeak-ng/espeak-ng
echo "Just what do you think you're doing, Dave?" | festival --tts
https://packages.debian.org/jessie/festival-voice
https://home-assistant.io/components/tts.picotts/
https://elinux.org/RPi_Text_to_Speech_(Speech_Synthesis)
local C:\Program Files (x86)\eSpeak NG

https://chaosdertechnik.de/debian-aufraumen/

Threading C++ lesen 
https://www.codeproject.com/Article.aspx?tag=198374995511576643&_z=6057568

Raspberry Typ ermitteln
cat /proc/cpuinfo | grep Revision | cut -d ':' -f 2 | sed -e "s/ //g"
https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/

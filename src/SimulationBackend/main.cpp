#define	ELPP_DEFAULT_LOG_FILE "./logs/backend.log"

#include <iostream>
#include "../common/easylogging++.h"
#include "../common/utils.h"
#include "InterruptHandler.h"
#include "BackendService.h"

//https://stackoverflow.com/questions/34438357/cmake-one-build-directory-for-multiple-projects
//http://quabr.com/36648048/how-to-create-a-c-linux-program-for-multithreading
//https://github.com/ivanmejiarocha/micro-service/blob/master/source/main.cpp
//http://headerphile.com/sdl2/sdl2-part-13-multiplayer-tcp/
//https://github.com/wjwwood/serial

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv) {
    std::cout << "Starting Simulation Backend" << std::endl;
    
    START_EASYLOGGINGPP(argc, argv);
    if(file_exists("loggerback.conf")) {
        // Load configuration from file
	    const el::Configurations conf("loggerback.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    }
    
    LOG(INFO) << "Starting Service";
    auto service = new BackendService();
    InterruptHandler::hookSIGINT();
    try {
        if(service->Start() == 0) {
            //InterruptHandler::waitForUserInterrupt();
            auto exitLoop = false;
            while(!exitLoop){
                std::cout << "Please enter your Command" << std::endl;
                std::string input;
                getline(std::cin, input);
                if(input == "end"){
                     std::cout << "Bye bye" << std::endl;
                     exitLoop = true;
                }
                else if(input == "exit"){
                     std::cout << "Bye bye" << std::endl;
                     exitLoop = true;
                }
                else if(input == "geo"){
                     std::cout << "Sending Geopos" << std::endl;
                     service->SendGeopos();
                }
                else if(input == "geo2"){
                     std::cout << "Sending Geopos" << std::endl;
                     service->SendGeopos2();
                }
                else if(input == "geof"){
                     std::cout << "Sending Geopos" << std::endl;
                     service->SendGeofile();
                }
            }
        }
        LOG(INFO) << "Stopping Service";
        service->Stop();
    }
    catch(std::exception& e) {
        LOG(ERROR) << "Error ";
    }
    /*catch(...) {
        RuntimeUtils::printStackTrace();
    }*/
    delete service;
    
    LOG(INFO) << "Service Stopped";
    return 0;
}

#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "InterruptHandler"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../common/easylogging++.h"
#include "InterruptHandler.h"

void InterruptHandler::handleUserInterrupt(int signal) {
    if (signal == SIGINT) {
        LOG(DEBUG) << "SIGINT trapped ...";
        _condition.notify_one();
    }
    if (signal == SIGTERM) {
        LOG(DEBUG) << "SIGTERM trapped ...";
        _condition.notify_one();
    }
}

void InterruptHandler::hookSIGINT() {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    signal(SIGINT, handleUserInterrupt);
    signal(SIGTERM, handleUserInterrupt);
    LOG(DEBUG) << "Signals Set";
}

void InterruptHandler::waitForUserInterrupt() {
    std::unique_lock<std::mutex> lock { _mutex };
    _condition.wait(lock);
    LOG(DEBUG) << "user has signaled to interrup program...";
    lock.unlock();
}
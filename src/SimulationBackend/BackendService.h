#pragma once

#include <thread>
#include <vector>
#if defined(_MSC_VER)
	#include <SDL_net.h>
#else
	#include <SDL2/SDL_net.h>
#endif
#include "../common/ClientSocketConnection.h"
#include "../common/json.hpp"

#if defined(_MSC_VER)
	#pragma warning(push)
	#pragma warning(disable:4251)
#endif

#include <osmscout/Database.h>

#if defined(_MSC_VER)
	#pragma warning(pop)
#endif


using json = nlohmann::json;

class BackendService
{
    IPaddress serverIp_;
    TCPsocket server_;
    bool run_;
    std::thread loop_thread_;
    std::vector<std::unique_ptr<ClientSocketConnection>> clientConnections_;
    SDLNet_SocketSet socketSet_;
    void Loop();
    void UpdateSocketSet();
    void IncomingMessage(const std::string& MessageName, json const& Message) const;
public:
    BackendService();
    ~BackendService();
    int Start();
    void Stop();
    void SendGeopos();
    void SendGeopos2();
    void SendGeofile();
};


#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ConfigManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../common/easylogging++.h"
#include "../common/json.hpp"

#include "ConfigManager.h"
#include <SDL2/SDL.h>
#ifdef _WIN32
#include <io.h> 
#define access    _access_s
#else
#include <unistd.h>
#endif

//Todo move to right place
namespace utils
{
    /**
     *  \brief Check File Extis Fast
     *
     *  \param [in] Filename the Filename
     *  \return bool exists
     *
     *  \details Check File Exists via Rigths
     */
    bool FileExists(const std::string &Filename)
    {
        return access(Filename.c_str(), 0) == 0;
    }
}

using json = nlohmann::json;

void to_json(json& j, const ConfigFile& p) {
    j = json {
        {"MapDataPath", p.mapDataPath},
        {"MapStyle", p.mapStyle},
        {"MapIconPaths", p.mapIconPaths},
        {"MediathekBasePath", p.mediathekBasePath}
    };
}

void from_json(const json& j, ConfigFile& p) {
    p.mapDataPath = j.at("MapDataPath").get<std::string>();
    p.mapStyle = j.at("MapStyle").get<std::string>();
    p.mapIconPaths = j.at("MapIconPaths").get<std::vector<std::string>>();
    auto it_value = j.find("MediathekBasePath");
    if(it_value != j.end()) {
        p.mediathekBasePath = j.at("MediathekBasePath").get<std::string>();
    } else {
        p.mediathekBasePath = "D:\\Mine\\";
    }
}

ConfigManager::ConfigManager()
{
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    appBasePath_ = "";
    appDataPath_ = "";
}

ConfigManager::~ConfigManager()
{
}

void ConfigManager::Init() {
	auto basePath = SDL_GetBasePath();
	if (basePath)
	{
		appBasePath_ = basePath;
		SDL_free(basePath);
	}

	basePath = SDL_GetPrefPath("MiNeSoftware", "CarNiNe");
	if (basePath) {
		appDataPath_ = basePath;
		SDL_free(basePath);
	}

	LOG(INFO) << "AppBasePath " << appBasePath_;
	LOG(INFO) << "AppDataPath " << appDataPath_;

    filenameConfig_ = appDataPath_ + "config.json";
    LOG(INFO) << "filenameConfig_ " << filenameConfig_;

    if(utils::FileExists(filenameConfig_)) {
        try {
            std::ifstream ifs(filenameConfig_);
            auto jConfig = json::parse(ifs);
            configFile_ = jConfig;
            ifs.close();
        /* Todo fix the compiler error
         * } catch(json::parse_error& exp) {
            LOG(ERROR) << exp.what();*/
        } catch(std::domain_error& exp) {
            LOG(ERROR) << exp.what();
        } catch(std::exception& exp) {
            LOG(ERROR) << exp.what();
        }
    } else {
        LOG(DEBUG) << "no config found generate default";
        configFile_.mapDataPath = R"(D:\Mine\OpenSource\libosmscout-code\maps\hessen-latest)";
        configFile_.mapStyle = R"(D:\Mine\OpenSource\libosmscout-code\stylesheets\standard.oss)";
        configFile_.mapIconPaths.emplace_back(
	        R"(D:\Mine\OpenSource\libosmscout-code\libosmscout\data\icons\14x14\standard\)");
        configFile_.mediathekBasePath = "D:\\Mine\\";
    }
}

void ConfigManager::Shutdown() {
    std::ofstream o(filenameConfig_);
	const json jConfig = configFile_;
    o << std::setw(4) << jConfig << std::endl;
    o.close();
}

std::string ConfigManager::GetDataPath() {
	return appDataPath_;
}

std::string ConfigManager::GetMapDataPath() {
    return configFile_.mapDataPath;
}

std::string ConfigManager::GetMapStylePath() {
    return configFile_.mapStyle;
}

std::vector<std::string> ConfigManager::GetMapIconPaths() {
    return configFile_.mapIconPaths;
}

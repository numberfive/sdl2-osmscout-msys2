#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "MapPainter"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../common/easylogging++.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#ifdef CAIRO_HAS_GL_SURFACE
    #include <cairo-gl.h>
#endif
#include "SDLEventManager.h"
#include "ConfigManager.h"
#include "MapPainter.h"
#include "../common/osmsoutlogger.h"

#define REDRAW 1
#define BREAK 2

MapPainter::MapPainter(SDLEventManager* eventManager, SDL_Renderer* renderer, float screenDpi):
	painter_(nullptr),
	mapAngle_(0) {
	eventManager_ = eventManager;
	renderer_ = renderer;
	mapEvent_ = -1;
	mapMemLock_ = SDL_CreateMutex();
	screenDpi_ = screenDpi;
	el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
	renderJobRun_ = false;
	mapTexture_ = nullptr;
	osmscout::log.SetLogger(new utils::Osmsoutlogger());

	image_data_source_ = nullptr;
	cairoImage_ = nullptr;
	paintMarker_ = false;
	image_data_marker_ = nullptr;
	currentSpeed_ = 0;
	carAngle_ = 0;
	markerTexture_ = nullptr;
	speedChange_ = 0;
}

MapPainter::~MapPainter() {
    if(cairoImage_ != nullptr){
        cairo_destroy(cairoImage_);
    }
    if(image_data_source_ != nullptr){
        cairo_surface_destroy(image_data_source_);
    }
}

Uint32 MapPainter::Init(int width, int height, ConfigManager* config) {
    double        lat=50.094,lon=8.49617;

    auto map = config->GetMapDataPath();
    auto style = config->GetMapStylePath();
    std::list<std::string> paths;
    auto iconPath = config->GetMapIconPaths();
    std::copy(iconPath.begin(), iconPath.end(), std::back_inserter( paths ) );

    _database.reset(new osmscout::Database(_databaseParameter));
    if (!_database->Open(map)) {
        std::cerr << "Cannot open database" << std::endl;
        return 0xFFFFFFFF;
    }

    _mapService.reset(new osmscout::MapService(_database));
    //locationService_.reset(new osmscout::LocationService(database_));

    styleConfig_.reset(new osmscout::StyleConfig(_database->GetTypeConfig()));
    if (!styleConfig_->Load(style)) {
        std::cerr << "Cannot open style" << std::endl;
        return 0xFFFFFFFF;
    }
    
    //This block is copy i don't Understand everythink hier
    //
    drawParameter_.SetIconPaths(paths);
    drawParameter_.SetPatternPaths(paths);
    drawParameter_.SetFontSize(3.0);
    drawParameter_.SetDebugData(false);
    drawParameter_.SetDebugPerformance(true);
    // We want to get notified, if we have more than 1000 objects from a certain type (=> move type rendering to a higher zoom level?)
    //drawParameter_.SetWarningObjectCountLimit(1000);
    // We want to get notified, if we have more than 20000 coords from a certain type (=> move type rendering to a higher zoom level?)
    //drawParameter_.SetWarningCoordCountLimit(20000);
    drawParameter_.SetRenderBackground(true); // we draw background before MapPainter
    drawParameter_.SetRenderUnknowns(false); // it is necessary to disable it with multiple databases
    drawParameter_.SetFontName("Inconsolata");// with an without Autobahn nummern fehlen
    searchParameter_.SetUseMultithreading(true);
    //searchParameter_.SetUseLowZoomOptimization(true);
    breaker_ = std::make_shared<osmscout::ThreadedBreaker>();
    drawParameter_.SetBreaker(breaker_);
    //drawParameter_.SetLabelLineMinCharCount(15);
    //drawParameter_.SetLabelLineMaxCharCount(30);
    //drawParameter_.SetLabelLineFitToArea(true);
    //drawParameter_.SetLabelLineFitToWidth(std::min(width_, height_));
    
    magnification_.SetLevel(osmscout::Magnification::magBlock);
    mapCenter_ = osmscout::GeoCoord(lat,lon);
    mapCenterJobStart_ = mapCenter_;
    carPos_ = mapCenter_;

    projectionSearch_.Set(mapCenter_,
                        mapAngle_,
                        magnification_,
                        screenDpi_,
                        width_,
                        height_);

      
    painter_ = new osmscout::MapPainterCairo(styleConfig_);

    width_ = width;
    height_ = height;

    mapWidth_ = width * 2;
    mapHeight_ = height * 2;

    startPoint_.x = 0;
    startPoint_.y = 0;

    viewDeltaNow_.x = 0;
    viewDeltaNow_.y = 0;

    viewDeltaAtJob_.x = viewDeltaNow_.x;
    viewDeltaAtJob_.y = viewDeltaNow_.y;

    mapTexture_ = SDL_CreateTexture(renderer_, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, mapWidth_, mapHeight_);
    mapPixels_ = new unsigned char[mapWidth_ * mapHeight_ * 4];

    mapLock_ = SDL_CreateSemaphore(0);
    SDL_AtomicSet(&done, 0);

    LOG(DEBUG) << "start background worker";

    mapEvent_ = SDL_RegisterEvents(1);

    mapBackgroundWorker_ = std::thread(&MapPainter::RendererThreadMain, this);
    SDL_SemPost(mapLock_);

    return mapEvent_;
}

int MapPainter::RendererThreadMain() {
    while (!SDL_AtomicGet(&done)) {
        SDL_SemWait(mapLock_);

        if(SDL_AtomicGet(&done)) break;

        RenderMap();
        if(drawParameter_.IsAborted()) {
            LOG(DEBUG) << "Map not rendered";
            BreakEvent();
        } else {
            LOG(DEBUG) << "Map rendered";
            RedrawEvent();
        }
    }
    LOG(DEBUG) << "Stop Background Thread";
    return 0;
}

void MapPainter::LoadMapData() {

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    TIMED_SCOPE(timerBlkObjLoadData, "load data");
#endif

    projectionDraw_.Set(mapCenterJobStart_,
                    mapAngle_,
                    magnification_,
                    screenDpi_,
                    mapWidth_,
                    mapHeight_);

    if (magnification_.GetLevel()>=15) {
        searchParameter_.SetMaximumAreaLevel(6);
    }
    else {
        searchParameter_.SetMaximumAreaLevel(4);
    }

    projectionDraw_.SetLinearInterpolationUsage(magnification_.GetLevel() >= 10);

    osmscout::StopClock clearDataTimer;
    //ClearUnusedData();
    data_.ClearDBData();
    clearDataTimer.Stop();

    LOG(INFO) << "clearDataTimer in " << clearDataTimer.GetMilliseconds();

    _mapService->LookupTiles(projectionDraw_, mapTiles_);
    auto count = 0;
    for (auto& mapTile : mapTiles_) {
        if (!mapTile.get()->IsComplete()) {
            count++;
        }
    }
    LOG(INFO) << "titles " << mapTiles_.size() << " size need load " << count;

    if(_mapService->LoadMissingTileData(searchParameter_, *styleConfig_, mapTiles_)) {
        osmscout::StopClock addTileDataTimer;
        _mapService->AddTileDataToMapData(mapTiles_, data_);
        addTileDataTimer.Stop();
        LOG(INFO) << "AddTileDataToMapData in " << addTileDataTimer.GetMilliseconds();
    }
}

void MapPainter::RenderMap() {

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    //TIMED_FUNC(RenderMapTimer);
    TIMED_SCOPE_IF(timerBlkObjclear, "clear", VLOG_IS_ON(4));
#endif

    LoadMapData();

#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    //TIMED_SCOPE(timerBlkObjCreateCairo, "create cairo");
    TIMED_SCOPE_IF(timerBlkObjload, "load", VLOG_IS_ON(4));
#endif

    //SDL_LockMutex(mapMemLock_);

    if(image_data_source_ == nullptr){
        image_data_source_ = cairo_image_surface_create_for_data(mapPixels_, CAIRO_FORMAT_ARGB32, mapWidth_, mapHeight_, cairo_format_stride_for_width (CAIRO_FORMAT_ARGB32, mapWidth_));
        cairoImage_ = cairo_create(image_data_source_);
        if (cairoImage_ == nullptr) {
            LOG(ERROR) << "cairoImage == nullptr";
            return;
        }
    }
{
#ifdef ELPP_FEATURE_PERFORMANCE_TRACKING
    TIMED_SCOPE_IF(timerBlkObjDrawData, "draw data", VLOG_IS_ON(4));
#endif

    std::string   output = "/home/punky/develop/sdl2-osmscout-msys2/bin/test.png";
    
    if (painter_->DrawMap(projectionDraw_, drawParameter_, data_, cairoImage_)) {
        LOG(DEBUG) << "Map drawed on surface";
        /*if(paintMarker_) {
            double x, y;
            projectionDraw_.GeoToPixel(projectionDraw_.GetCenter(), x, y);
            if(image_data_marker_ == nullptr) {
                image_data_marker_ = cairo_image_surface_create_from_png("auto-day-icon.png");
            }
            auto imageWidth = cairo_image_surface_get_width(image_data_marker_);
            auto imageHeight = cairo_image_surface_get_height(image_data_marker_);
            cairo_set_source_surface(cairoImage_, image_data_marker_, x - (imageWidth / 2), y - (imageHeight / 2));
            cairo_paint(cairoImage_);
            LOG(DEBUG) << "drawed marker on surface";
        }
        if (cairo_surface_write_to_png(image_data_source_,output.c_str())!=CAIRO_STATUS_SUCCESS) {
            LOG(ERROR) << "Cannot write PNG";
        }*/
        cairo_surface_flush(image_data_source_);
    }
}

    
    //cairo_destroy(cairoImage);
    //cairo_surface_destroy(image_data_source);

    //SDL_UnlockMutex(mapMemLock_);
}

void MapPainter::ClearUnusedData() {
    auto nodescount = 0;
	auto areascount = 0;
	auto wayscount = 0;
	double minLat;
	double minLon;
	if(!projectionDraw_.PixelToGeo(0, 0, minLat, minLon)) {
		LOG(WARNING) << "PixelToGeo failed";
	}
	double maxLat;
	double maxLon;
	if(!projectionDraw_.PixelToGeo(mapWidth_, mapHeight_, maxLat, maxLon)) {
		LOG(WARNING) << "PixelToGeo failed";
	}
	const osmscout::GeoBox currentBox(osmscout::GeoCoord(minLat, minLon), osmscout::GeoCoord(maxLat, maxLon));

	for (auto it = data_.nodes.begin(); it != data_.nodes.end(); /*noting*/) {
		if (!it->get()->Intersects(currentBox)) {
			it = data_.nodes.erase(it);
			nodescount++;
		}
		else {
			++it;
		}
	}

	LOG(INFO) << nodescount << " nodes deleted";

	for (auto it = data_.areas.begin(); it != data_.areas.end(); /*noting*/) {
		if (!it->get()->Intersects(currentBox)) {
			it = data_.areas.erase(it);
			areascount++;
		}
		else {
			++it;
		}
	}

	LOG(INFO) << areascount << " areas deleted";

	for (auto it = data_.ways.begin(); it != data_.ways.end(); /*noting*/) {
		if (!it->get()->Intersects(currentBox)) {
			it = data_.ways.erase(it);
			wayscount++;
		}
		else {
			++it;
		}
	}

	LOG(INFO) << wayscount << " ways deleted";
}

void MapPainter::RedrawEvent() const {
    SDL_Event event;
    SDL_memset(&event, 0, sizeof(event));
    event.type = mapEvent_;
    event.user.code = REDRAW;
    event.user.type = mapEvent_;
    SDL_PushEvent(&event);
}

void MapPainter::BreakEvent() const {
    SDL_Event event;
    SDL_memset(&event, 0, sizeof(event));
    event.type = mapEvent_;
    event.user.code = BREAK;
    event.user.type = mapEvent_;
    SDL_PushEvent(&event);
}

void MapPainter::HandleEvent(const SDL_Event& event) {
    if(event.type == mapEvent_) {
        HandleMapEvent(event);
    } else {
        switch(event.type){
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEMOTION:
            case SDL_MOUSEBUTTONUP:
            case SDL_MOUSEWHEEL:
                HandleMouseEvent(event);
                break;
        }
    }
}

void MapPainter::HandleMapEvent(const SDL_Event& event){
    if(event.user.code == REDRAW) {
        LOG(DEBUG) << "Redraw Event";
        int w, h;
        SDL_QueryTexture(mapTexture_, nullptr, nullptr, &w, &h);
        if(w != mapWidth_ || h != mapHeight_){
            SDL_DestroyTexture(mapTexture_);
            mapTexture_ = SDL_CreateTexture(renderer_, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, mapWidth_, mapHeight_);
        }
        
        LOG(DEBUG) << "new map get job x " << viewDeltaAtJob_.x << " y " << viewDeltaAtJob_.y;
        LOG(DEBUG) << "new map get screen x " << viewDeltaNow_.x << " y " << viewDeltaNow_.y;
        LOG(DEBUG) << "center on start job " << mapCenterJobStart_.GetDisplayText();
        
        mapCenter_ = mapCenterJobStart_;
        projectionSearch_.Set(mapCenter_,
                              mapAngle_,
                              magnification_,
                              screenDpi_,
                              mapWidth_,
                              mapHeight_);

        /*double x,y;
        if(projectionSearch_.GeoToPixel(carPos_, x, y)){
            LOG(DEBUG) << "need to move to " << x << "," << y;
            viewDeltaNow_.x = (mapWidth_/2) - (int)x;
            viewDeltaNow_.y = (mapHeight_/2) - (int)y;
        }*/
                
        viewDeltaNow_.x = viewDeltaNow_.x - viewDeltaAtJob_.x;
        viewDeltaNow_.y = viewDeltaNow_.y - viewDeltaAtJob_.y;

        LOG(DEBUG) << "new map move map screen x " << viewDeltaNow_.x << " y " << viewDeltaNow_.y;
		if(currentSpeed_ <= 10) {
			if(viewDeltaNow_.x != 0 || viewDeltaNow_.y != 0) {
				LOG(DEBUG) << "reset simulated map move";
				viewDeltaNow_.x = 0;
				viewDeltaNow_.y = 0;
			}
		}

        SDL_LockMutex(mapMemLock_);
        void *pixels;
        int pitch;
        if(SDL_LockTexture(mapTexture_, nullptr, &pixels, &pitch) == 0) {
            memcpy(pixels, mapPixels_, pitch * mapHeight_);
            SDL_UnlockTexture(mapTexture_);
        } else {
            LOG(ERROR) << SDL_GetError();
        }
        //SDL_UpdateTexture(mapTexture_, nullptr, mapPixels_, 4 * mapWidth_);
        SDL_UnlockMutex(mapMemLock_);
        renderJobRun_ = false;
        lastUpdate_ = lastUpdateJobStart_;
        return;
        
        projectionSearch_.Set(mapCenter_,
                              mapAngle_,
                              magnification_,
                              screenDpi_,
                              mapWidth_,
                              mapHeight_);
        if(viewDeltaNow_.x == 0 && viewDeltaNow_.y == 0) {
            renderJobRun_ = false;
        } else {

            viewDeltaAtJob_.x = viewDeltaNow_.x;
            viewDeltaAtJob_.y = viewDeltaNow_.y;

            LOG(DEBUG) << "new map move map to x " << viewDeltaNow_.x << " y " << viewDeltaNow_.y;

            if(projectionSearch_.Move( -viewDeltaNow_.x, viewDeltaNow_.y)) {
                mapCenterJobStart_  = projectionSearch_.GetCenter();
                SDL_SemPost(mapLock_);
            } else {
                LOG(WARNING) << "failed get center";
                renderJobRun_ = false;
            }
        }
    } else if(event.user.code == BREAK) {
        breaker_->Reset();
        LOG(DEBUG) << "map repaint stopped";

        projectionSearch_.Set(mapCenterJobStart_,
                                  mapAngle_,
                                  magnification_,
                                  screenDpi_,
                                  mapWidth_,
                                  mapHeight_);

        mapCenter_  = projectionSearch_.GetCenter();
        LOG(DEBUG) << "pos at break job " <<  mapCenter_.GetDisplayText();
       
        if(viewDeltaNow_.x == 0 && viewDeltaNow_.y == 0) {
            renderJobRun_ = false;
        } else {

            viewDeltaAtJob_.x = viewDeltaNow_.x;
            viewDeltaAtJob_.y = viewDeltaNow_.y;

            LOG(DEBUG) << "new map move map to x " << viewDeltaNow_.x << " y " << viewDeltaNow_.y;

            if(projectionSearch_.Move( -viewDeltaNow_.x, viewDeltaNow_.y)) {
                mapCenter_  = projectionSearch_.GetCenter();
                SDL_SemPost(mapLock_);
            } else {
                LOG(WARNING) << "failed get center";
                renderJobRun_ = false;
            }
        }
    }
}

void MapPainter::HandleMouseEvent(const SDL_Event& event) {
    if(event.type == SDL_MOUSEBUTTONDOWN) {
	    const auto mouseEvent = event.button;
        startPoint_.x = mouseEvent.x;
        startPoint_.y = mouseEvent.y;
    }
    if(event.type == SDL_MOUSEMOTION) {
	    const auto mouseEvent = event.motion;
        if(mouseEvent.state & SDL_BUTTON_LMASK) {
            
            int dx = mouseEvent.x - startPoint_.x;
            int dy = mouseEvent.y - startPoint_.y;

            viewDeltaNow_.x = viewDeltaNow_.x + dx;
            viewDeltaNow_.y = viewDeltaNow_.y + dy;

            startPoint_.x = mouseEvent.x;
            startPoint_.y = mouseEvent.y;

            if(!renderJobRun_) {

                LOG(DEBUG) << "move map to x " << viewDeltaNow_.x << " y " << viewDeltaNow_.y;
                LOG(DEBUG) << "dx " << dx << " dy " << dy;

                renderJobRun_ = true;
                viewDeltaAtJob_.x = viewDeltaNow_.x;
                viewDeltaAtJob_.y = viewDeltaNow_.y;
                mapCenterJobStart_ = mapCenter_;

                LOG(DEBUG) << "pos at start job" << mapCenter_.GetDisplayText();

                if(projectionSearch_.Move( -viewDeltaNow_.x, viewDeltaNow_.y)) {
                    mapCenterJobStart_  = projectionSearch_.GetCenter();
                    SDL_SemPost(mapLock_);
                } else {
                    LOG(WARNING) << "failed get center";
                }
            } else {
                //breaker_->Break();
            }
        }
    }
    if(event.type == SDL_MOUSEBUTTONUP) {
        /*auto mouseEvent = event.button;
        int dx = startPoint_.x - mouseEvent.x;
        int dy = mouseEvent.y - startPoint_.y;

        if(projection_.Move(dx,dy)) {
            mapCenter_  = projection_.GetCenter();
            SDL_SemPost(mapLock_);
        } else {
            LOG(WARNING) << "failed get center";
        }*/

    }
    if(event.type == SDL_MOUSEWHEEL) {
         LOG(DEBUG) << "SDL_MOUSEWHEEL x " << event.wheel.x << " y "<< event.wheel.y;
         if(event.wheel.y == -1){
             int level = magnification_.GetLevel();
             level--;
             if( level >= 0 ){
                 magnification_.SetLevel(osmscout::Magnification::magBlock);

                 renderJobRun_ = true;
                 viewDeltaAtJob_.x = viewDeltaNow_.x;
                 viewDeltaAtJob_.y = viewDeltaNow_.y;

                if(projectionSearch_.Move( -viewDeltaNow_.x, viewDeltaNow_.y)) {
                    mapCenter_  = projectionSearch_.GetCenter();
                    SDL_SemPost(mapLock_);
                }
             }
         }
         if(event.wheel.y == +1){
             int level = magnification_.GetLevel();
             level++;
             if( level <= 20 ){
                 magnification_.SetLevel(osmscout::Magnification::magBlock);

                 renderJobRun_ = true;
                 viewDeltaAtJob_.x = viewDeltaNow_.x;
                 viewDeltaAtJob_.y = viewDeltaNow_.y;

                if(projectionSearch_.Move( -viewDeltaNow_.x, viewDeltaNow_.y)) {
                    mapCenter_  = projectionSearch_.GetCenter();
                    SDL_SemPost(mapLock_);
                }
             }
         }
    }
}

void MapPainter::Draw() {

    if(mapTexture_ == nullptr) return;
    
    SDL_Rect drawRect;
    drawRect.x = viewDeltaNow_.x - (mapWidth_ / 4);
    drawRect.y = viewDeltaNow_.y - (mapHeight_ / 4);
    drawRect.w = mapWidth_;
    drawRect.h = mapHeight_;

    SDL_RenderCopy(renderer_, mapTexture_, nullptr, &drawRect);
    
    if(paintMarker_) {
        if(markerTexture_ == nullptr) {
            markerTexture_ = IMG_LoadTexture(renderer_, "auto-day-icon.png");
            int w, h; // texture width & height
            SDL_QueryTexture(markerTexture_, nullptr, nullptr, &w, &h);
            markerImageSize_.x = w;
            markerImageSize_.y = h;
        }
        if (markerTexture_ != nullptr) {
            drawRect.x = (width_ / 2) - (markerImageSize_.x / 2);
            drawRect.y = (height_ / 2) - (markerImageSize_.y / 2);
            drawRect.w = markerImageSize_.x;
            drawRect.h = markerImageSize_.y;
            SDL_RenderCopy(renderer_, markerTexture_, nullptr, &drawRect);
        }
    }
}

void MapPainter::Resize(int width, int height) {

    if(width == width_ && height == height_) return;

    if(cairoImage_ != nullptr){
        cairo_destroy(cairoImage_);
        cairoImage_ = nullptr;
    }
    
    if(image_data_source_ != nullptr){
        cairo_surface_destroy(image_data_source_);
        image_data_source_ = nullptr;
    }
    
    width_ = width;
    height_ = height;

    mapWidth_ = width * 2;
    mapHeight_ = height * 2;

    startPoint_.x = 0;
    startPoint_.y = 0;

    viewDeltaNow_.x = 0;
    viewDeltaNow_.y = 0;

    viewDeltaAtJob_.x = viewDeltaNow_.x;
    viewDeltaAtJob_.y = viewDeltaNow_.y;

    SDL_LockMutex(mapMemLock_);

    projectionSearch_.Set(mapCenter_,
                          mapAngle_,
                          magnification_,
                          screenDpi_,
                          mapWidth_,
                          mapHeight_);

    delete[] mapPixels_;
    mapPixels_ = new unsigned char[mapWidth_ * mapHeight_ * 4];

    SDL_UnlockMutex(mapMemLock_);

    SDL_SemPost(mapLock_);
}

void MapPainter::CenterMap(const osmscout::GeoCoord& coord, const double& compass, const double& speed) {
    if (compass > -1) {
        //double angleDeg = (360*(angle/(2*M_PI))); found in code
        // full turn is 2*PI
        carAngle_ = compass;
        mapAngle_ = -compass * (2.0 * M_PI) / 360.0;
    }
    if(speed > -1) {
		if(speed < currentSpeed_) {
			speedChange_ = (currentSpeed_ - speed) / 3.6;
			LOG(INFO) << "slow down";
		} else {
			speedChange_ = 0;
		}
        currentSpeed_ = speed;
        if(speed == 0) {
            carPos_ = coord;
        }
    }
        
    paintMarker_ = true;
	if (_streamGpxFile.is_open()) {
		_streamGpxFile << "\t\t\t<trkpt lat=\"" << coord.GetLat() << "\" lon=\"" << coord.GetLon() << "\">" << std::endl;
		//_streamGpxFile << "\t\t\t\t<time>" << osmscout::TimestampToISO8601TimeString(point.time) << "</time>" << std::endl;
		_streamGpxFile << "\t\t\t\t<speed>" << currentSpeed_ / 3.6 << "</speed>" << std::endl;
		_streamGpxFile << "\t\t\t\t<fix>2d</fix>" << std::endl;
		_streamGpxFile << "\t\t\t</trkpt>" << std::endl;
	} else {
		_positionsThink.clear();

		_streamGpxFile.open("routeLife.gpx", std::ofstream::trunc);
		_streamGpxFile.precision(8);
		_streamGpxFile << R"(<?xml version="1.0" encoding="UTF-8" standalone="no" ?>)" << std::endl;
		_streamGpxFile << R"(<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="TestNavLibOsmScout" version="0.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">)" << std::endl;

		_streamGpxFile << "\t<wpt lat=\"" << coord.GetLat() << "\" lon=\"" << coord.GetLon() << "\">" << std::endl;
		//_streamGpxFile << "\t\t<name>Route lost " << std::to_string(_errorCount) << "</name>" << std::endl;
		_streamGpxFile << "\t\t<fix>2d</fix>" << std::endl;
		_streamGpxFile << "\t</wpt>" << std::endl;

		_streamGpxFile << "\t<trk>" << std::endl;
		_streamGpxFile << "\t\t<name>GPS</name>" << std::endl;
		_streamGpxFile << "\t\t<number>1</number>" << std::endl;
		_streamGpxFile << "\t\t<trkseg>" << std::endl;

		_streamGpxFile << "\t\t\t<trkpt lat=\"" << coord.GetLat() << "\" lon=\"" << coord.GetLon() << "\">" << std::endl;
		//_streamGpxFile << "\t\t\t\t<time>" << osmscout::TimestampToISO8601TimeString(point.time) << "</time>" << std::endl;
		_streamGpxFile << "\t\t\t\t<speed>" << currentSpeed_ << "</speed>" << std::endl;
		_streamGpxFile << "\t\t\t\t<fix>2d</fix>" << std::endl;
		_streamGpxFile << "\t\t\t</trkpt>" << std::endl;
	}

    //mapCenter_ = coord;

    viewDeltaAtJob_.x = viewDeltaNow_.x;
    viewDeltaAtJob_.y = viewDeltaNow_.y;
    
    mapCenterJobStart_ = coord;
    LOG(DEBUG) << "CenterMap" << mapCenterJobStart_.GetDisplayText();
        
    projectionSearch_.Set(mapCenter_,
                          mapAngle_,
                          magnification_,
                          screenDpi_,
                          mapWidth_,
                          mapHeight_);
    renderJobRun_ = true;
    lastUpdateJobStart_ = std::chrono::high_resolution_clock::now();
    SDL_SemPost(mapLock_);
    
}

void MapPainter::DoUpdateIdle() {
	if (currentSpeed_ < 10) return;

	const auto timeCurrent = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> delta( timeCurrent - lastUpdate_);

	VLOG(3) << "DoUpdateIdle " << delta.count();
    auto speedToDistance = currentSpeed_ / 3.6; //m/sec wenn speed km/h
    speedToDistance = speedToDistance * delta.count() - speedChange_ * delta.count();
		
    if (speedToDistance <= 0.0) return;

    auto posIThink = mapCenter_.Add(osmscout::Bearing::Degrees(carAngle_),osmscout::Distance::Of<osmscout::Meter>(speedToDistance));
	VLOG(3) << "CenterMap after " << delta.count() << " seconds " << posIThink.GetDisplayText();
    double x;
    double y;
    if(projectionSearch_.GeoToPixel(posIThink, x, y)) {
		_positionsThink.emplace_back(currentSpeed_, posIThink);
        carPos_ = posIThink;
		VLOG(3) << "need to move to " << x << "," << y;
        viewDeltaNow_.x = (mapWidth_/2) - static_cast<int>(x);
        viewDeltaNow_.y = (mapHeight_/2) - static_cast<int>(y);
		VLOG(3) << "need to move to in pixel " << viewDeltaNow_.x << "," << viewDeltaNow_.y;
        //lastUpdate_ = std::chrono::high_resolution_clock::now();
            
        /*SDL_LockMutex(mapMemLock_);
        unsigned char *pixels;
        int pitch;
        if(SDL_LockTexture(mapTexture_, NULL, (void**)&pixels, &pitch) == 0) {
            memcpy(pixels, mapPixels_, pitch * mapHeight_);
            cairo_surface_t *cs=cairo_image_surface_create_for_data(pixels,CAIRO_FORMAT_ARGB32, mapWidth_, mapHeight_, pitch);
            cairo_t* cr=cairo_create(cs);
            cairo_set_source_rgb (cr, 1, 0, 0);
            cairo_arc (cr, x, y, 40, 0, 2 * M_PI);
            cairo_stroke(cr);
            SDL_UnlockTexture(mapTexture_);
        } else {
            LOG(ERROR) << SDL_GetError();
        }*/
            
        /*viewDeltaNow_.x = mapWidth_ - x;
        viewDeltaNow_.y = mapHeight_ - y;*/
    } else {
		LOG(WARNING) << "I think pos not on map";
    }
    
}

void MapPainter::CloseFiles() {
	if (_streamGpxFile.is_open()) {

		_streamGpxFile << "\t\t</trkseg>" << std::endl;
		_streamGpxFile << "\t</trk>" << std::endl;

		_streamGpxFile << "\t<trk>" << std::endl;
		_streamGpxFile << "\t\t<name>Think</name>" << std::endl;
		_streamGpxFile << "\t\t<number>2</number>" << std::endl;
		_streamGpxFile << "\t\t<trkseg>" << std::endl;

		for (const auto& point : _positionsThink) {
			_streamGpxFile << "\t\t\t<trkpt lat=\"" << point.coord.GetLat() << "\" lon=\"" << point.coord.GetLon() << "\">" << std::endl;
			//stream << "\t\t\t\t<time>" << osmscout::TimestampToISO8601TimeString(point.time) << "</time>" << std::endl;
			_streamGpxFile << "\t\t\t\t<speed>" << point.speed / 3.6 << "</speed>" << std::endl;
			_streamGpxFile << "\t\t\t\t<fix>2d</fix>" << std::endl;
			_streamGpxFile << "\t\t\t</trkpt>" << std::endl;
		}

		_streamGpxFile << "\t\t</trkseg>" << std::endl;
		_streamGpxFile << "\t</trk>" << std::endl;

		_streamGpxFile << "\t<wpt lat=\"" << carPos_.GetLat() << "\" lon=\"" << carPos_.GetLon() << "\">" << std::endl;
		//_streamGpxFile << "\t\t<name>Route lost " << std::to_string(_errorCount) << "</name>" << std::endl;
		_streamGpxFile << "\t\t<fix>2d</fix>" << std::endl;
		_streamGpxFile << "\t</wpt>" << std::endl;

		_streamGpxFile << "</gpx>" << std::endl;
		_streamGpxFile.close();
	}
}
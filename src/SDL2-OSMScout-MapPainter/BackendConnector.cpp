#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "BackendConnector"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../common/easylogging++.h"
#include "../common/BackendMessages.h"
#include "BackendConnector.h"
#include "SDLEventManager.h"
#include "../common/ClientSocketConnection.h"

BackendConnector::BackendConnector(SDLEventManager* eventManager) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    eventManager_ = eventManager;
    connection_ = new ClientSocketConnection("127.0.0.1", SERVERPORT);
    socketSet_ = nullptr;
    run_ = true;
    backendEventType_ = eventManager_->GetNewEventType();
}

BackendConnector::~BackendConnector() {
    if(connection_ != nullptr){
        delete connection_;
    }
}

Uint32 BackendConnector::Init() {
    loop_thread_ = std::thread(&BackendConnector::Loop, this);
    return backendEventType_;
}

void BackendConnector::Shutdown() {
    LOG(DEBUG) << "Shutdown";
    run_ = false;
    connection_->CloseSocket();
    loop_thread_.join();
}

void BackendConnector::Loop() {
    while(run_) {
        if(!connection_->IsConnected()){
            if(!connection_->OpenConnectionToServer()) {
                SDL_Delay(500);
                continue;
            } else {
                if(socketSet_ != nullptr) {
                    SDLNet_FreeSocketSet(socketSet_);
                }
                socketSet_ = SDLNet_AllocSocketSet(1);
                connection_->AddToSocketSet(&socketSet_);
                auto newDatadelegate = std::bind(&BackendConnector::IncomingMessage, this, std::placeholders::_1, std::placeholders::_2);
                connection_->SetNewDataCallback(newDatadelegate);
            }
        }
        
        int numready = SDLNet_CheckSockets(socketSet_, 1000);
        if(numready != 0) {
            auto needRead = false;
            if(connection_->CheckSocket(&needRead)) {
                if(needRead){
                    connection_->Read();
                }
            }
        }
    }
    
    delete connection_;
    connection_ = nullptr;
    
    SDLNet_FreeSocketSet(socketSet_);
    LOG(INFO) << "TCP thread is stopped";
}

void BackendConnector::IncomingMessage(const std::string& MessageName, json const& Message) {
     LOG(DEBUG) << "Incoming Message " << MessageName;
     if(MessageName == "GPSMessage") {
         GPSMessage messageJson = Message;
         auto intMessage = new InternalGPSMessage(messageJson);
         
         eventManager_->PushEvent(backendEventType_, 0, 4, intMessage, nullptr);
     }
}
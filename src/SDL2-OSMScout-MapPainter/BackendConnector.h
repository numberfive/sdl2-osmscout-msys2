#pragma once

#include <thread>
#include <SDL2/SDL_net.h>
#include "../common/json.hpp"
#include <osmscout/Database.h>
#include "../common/BackendMessages.h"

using json = nlohmann::json;

struct InternalGPSMessage {
    InternalGPSMessage() {
        
    }
    
    InternalGPSMessage(const GPSMessage& gpsMessage) {
        coord = osmscout::GeoCoord(gpsMessage.Latitude, gpsMessage.Longitude);
        compass = gpsMessage.Compass;
        speed = gpsMessage.Speed;
    }
    
    osmscout::GeoCoord coord;
    double compass;
    double speed;
};

class SDLEventManager;
class ClientSocketConnection;

class BackendConnector
{
    Uint32 backendEventType_;
    SDLEventManager* eventManager_;
    ClientSocketConnection* connection_;
    SDLNet_SocketSet socketSet_;
    bool run_;
    std::thread loop_thread_;
    void Loop();
    void IncomingMessage(const std::string& MessageName, json const& Message);
public:
    BackendConnector(SDLEventManager* eventManager);
    ~BackendConnector();
    
    Uint32 Init();
    void Shutdown();
};


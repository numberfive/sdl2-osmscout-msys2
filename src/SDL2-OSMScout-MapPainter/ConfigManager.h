#pragma once

struct ConfigFile {
    std::string mapDataPath;
    std::string mapStyle;
    std::vector<std::string> mapIconPaths;
    std::string mediathekBasePath;
};

class ConfigManager
{
    std::string appBasePath_;
    std::string appDataPath_;
    std::string filenameConfig_;
    ConfigFile configFile_;
public:
    ConfigManager();
    ~ConfigManager();

    void Init();
    void Shutdown();

    std::string GetDataPath();
    std::string GetMapDataPath();
    std::string GetMapStylePath();
    std::vector<std::string> GetMapIconPaths();
};


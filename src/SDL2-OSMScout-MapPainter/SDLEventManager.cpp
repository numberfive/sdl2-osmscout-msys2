#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "SDLEventManager"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif
#include "../common/easylogging++.h"
#include <SDL2/SDL.h>
#include "SDLEventManager.h"

/**
* @file  SDLEventManager.cpp
*
* Implementation for Managed SDLEvents Threadsave
*/

SDLEventManager::SDLEventManager():
    eventLock_(nullptr),
    eventWait_(nullptr)
{
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
}

SDLEventManager::~SDLEventManager()
{
    SDL_DestroyMutex(eventLock_);
    eventLock_ = nullptr;

    SDL_DestroyCond(eventWait_);
    eventWait_ = nullptr;
}

bool SDLEventManager::Init()
{
    eventLock_ = SDL_CreateMutex();
    if (eventLock_ == nullptr) {
        return false;
    }

    eventWait_ = SDL_CreateCond();
	return eventWait_ != nullptr;
}

Uint32 SDLEventManager::GetNewEventType()
{
    auto result = SDL_RegisterEvents(1);
    if (result == static_cast<Uint32>(-1)) {
        LOG(ERROR) << "register event faild";
    }
    return result;
}

bool SDLEventManager::PushEvent(Uint32 type, Uint32 windowID, Sint32 code, void* data1, void* data2) const {
    if(eventLock_ == nullptr) return false;

    SDL_LockMutex(eventLock_);
    SDL_Event event;
    SDL_memset(&event, 0, sizeof(event));
    event.type = type;
	event.user.code = code;
	event.user.type = type;
	event.window.windowID = windowID;
	event.user.windowID = windowID;
	event.user.data1 = data1;
	event.user.data2 = data2;
	const auto result = SDL_PushEvent(&event);
	if (result < 0) {
		SDL_UnlockMutex(eventLock_);
		LOG(ERROR) << "SDL_PushEvent faild";
	}
	else if (result == 0) {
		LOG(WARNING) << "Event filtered";
		SDL_UnlockMutex(eventLock_);
		return false;
	}
	else {
		LOG(DEBUG) << "Event Pushed";
	}
	SDL_UnlockMutex(eventLock_);
	SDL_CondSignal(eventWait_);
	return true;
}

int SDLEventManager::WaitEvent(SDL_Event* event, Uint32 timeout) const {
	auto result = 0;

	SDL_LockMutex(eventLock_);
	result = SDL_PollEvent(event);
	if (result == 0) {
		if (SDL_CondWaitTimeout(eventWait_, eventLock_, timeout) == SDL_MUTEX_TIMEDOUT) {
			result = 0;
		}
		else {
			result = SDL_PollEvent(event);
		}
	}
	SDL_UnlockMutex(eventLock_);
	SDL_CondSignal(eventWait_);

	return result;
}

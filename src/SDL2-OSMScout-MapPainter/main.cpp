#include <iostream>
#define	ELPP_DEFAULT_LOG_FILE "./logs/carnine.log"
#include <SDL2/SDL.h>
#include <SDL2/SDL_net.h>
#include <SDL2/SDL_image.h>
#include "../common/easylogging++.h"
#include "../common/utils.h"
#include "SDLEventManager.h"
#include "ConfigManager.h"
#include "MapPainter.h"
#include "BackendConnector.h"

const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 600;

#define MILLESECONDS_PER_FRAME (1000.0 / 60.0)       /* about 60 frames per second */

INITIALIZE_EASYLOGGINGPP

int main(int argc, char **argv) {
	
	START_EASYLOGGINGPP(argc, argv);
    if(file_exists("logger.conf")) {
        // Load configuration from file
	    const el::Configurations conf("logger.conf");
        // Now all the loggers will use configuration from file and new loggers
        el::Loggers::setDefaultConfigurations(conf, true);
    }

    LOG(DEBUG) << "Starting SDL";

    SDL_LogSetAllPriority(SDL_LOG_PRIORITY_VERBOSE);

    SDL_version compiled;
    SDL_version linked;

    SDL_VERSION(&compiled);
    SDL_GetVersion(&linked);

	auto versionText = std::to_string(linked.major);
	versionText += ".";
	versionText += std::to_string(linked.minor);
	versionText += ".";
	versionText += std::to_string(linked.patch);
	
    LOG(INFO) << "SDL Version compiled " << static_cast<int>(compiled.major) << "." << static_cast<int>(compiled.minor) << "." << static_cast<int>(compiled.patch);
    LOG(INFO) << "SDL Version linked " << static_cast<int>(linked.major) << "." << static_cast<int>(linked.minor) << "." << static_cast<int>(linked.patch);

    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        return 99;
    }

    if(SDLNet_Init() == -1) {
        LOG(ERROR) << SDLNet_GetError();
        return -1;
    }
    
    IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);
    /*if(IMG_Init() == -1) {
        LOG(ERROR) << SDLNet_GetError();
        return -1;
    }*/
    
    auto config = new ConfigManager();
    config->Init();

	std::string titel = "LibOsmTest with SDL2 Version " + versionText;
	const auto window = SDL_CreateWindow(titel.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI| SDL_WINDOW_HIDDEN );
    if( window == nullptr ) {
        std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 99;
    }

	const auto renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if( renderer == nullptr ) {
        SDL_DestroyWindow( window );
        SDL_Quit();
        std::cout << "renderer not initialize! SDL_Error: " << SDL_GetError() << std::endl;
        return 99;
    }

#ifdef CAIRO_HAS_GL_SURFACE
    SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
    SDL_GLContext _cairoContext;
    _cairoContext = SDL_GL_CreateContext(window);
#endif

    float ddpi, hdpi, vdpi;

    if(SDL_GetDisplayDPI(0, &ddpi, &hdpi, &vdpi) !=0) {
        //This happend on Oracle VirtualBox
        LOG(ERROR) << "get dpi failed " << SDL_GetError();
        ddpi = 96.0;
    }

    auto eventManager = new SDLEventManager();
    if(!eventManager->Init()) {
        SDL_DestroyWindow( window );
        SDL_Quit();
        std::cout << "eventManager init failed" << SDL_GetError() << std::endl;
        return 99;
    }

    auto mapPainter = new MapPainter(eventManager, renderer, ddpi);
    auto mapEvent = mapPainter->Init(SCREEN_WIDTH, SCREEN_HEIGHT, config);

    auto connector = new BackendConnector(eventManager);
    
    SDL_RenderPresent( renderer );

	auto quit = false;
    SDL_Event e;
    std::cout << "every thing create show window end enter main loop" << std::endl;

    SDL_ShowWindow(window);

	const auto backendEvent = connector->Init();
    
    auto delay = static_cast<int>(MILLESECONDS_PER_FRAME);
    //While application is running
    while( !quit ) {
	    const auto startFrame = SDL_GetTicks();
        auto noevent = true;
        while( eventManager->WaitEvent(&e, delay) != 0 ) {
            noevent = false;
            if( e.type == SDL_QUIT ) {
                quit = true;
            }

            mapPainter->HandleEvent(e);
            if(e.type == SDL_WINDOWEVENT) {
                switch (e.window.event) {
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                    case SDL_WINDOWEVENT_RESIZED:
                        mapPainter->Resize(e.window.data1, e.window.data2);
                    break;
                }
            }
            if(e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym)   
                {
                    case SDLK_m:
                        mapPainter->CenterMap(osmscout::GeoCoord(50.0908, 8.50699), 90.0, -1);
						break;
					case SDLK_k:
						mapPainter->CloseFiles();
						LOG(INFO) << "GPX Files written";
						break;
                }
            }
            if(e.type == backendEvent) {
                switch (e.user.code)   
                {
                    case 4:
						const auto message = static_cast<InternalGPSMessage*>(e.user.data1);
                        mapPainter->CenterMap(message->coord, message->compass, message->speed);
                        delete message;
                }
            }
        } 
        if(noevent){
            mapPainter->DoUpdateIdle();
        }

        //wipe the screen with green
        SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0, 255);
        SDL_RenderClear(renderer);

        mapPainter->Draw();
        SDL_RenderPresent( renderer );

	    const auto endFrame = SDL_GetTicks();
        delay = static_cast<int>(MILLESECONDS_PER_FRAME - (endFrame - startFrame));
        if (delay < 0) {
            delay = 0;
        }
        else if (delay > MILLESECONDS_PER_FRAME) {
            delay = static_cast<int>(MILLESECONDS_PER_FRAME);
        }
    }

    std::cout << "main loop leaved" << std::endl;

    config->Shutdown();
    connector->Shutdown();
    delete connector;
    delete config;
    delete eventManager;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDLNet_Quit();
    SDL_Quit();

    return 0;
}


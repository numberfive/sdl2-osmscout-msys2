#pragma once

#include <thread>
#include <chrono>
#if defined(_MSC_VER)
	#pragma warning(push)
	#pragma warning(disable:4251)
#endif

#include <osmscout/Database.h>
#include <osmscout/MapService.h>
#include <osmscout/MapPainterCairo.h>

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

class SDLEventManager;

struct Step
{
	//osmscout::Timestamp time;
	double              speed;
	osmscout::GeoCoord  coord;

	Step(double speed,
		const osmscout::GeoCoord& coord)
		: speed(speed),
		coord(coord)
	{
		// no code
	}
};

class MapPainter
{
    osmscout::DatabaseParameter   _databaseParameter;
    osmscout::DatabaseRef         _database;
    osmscout::MapServiceRef       _mapService;
    osmscout::StyleConfigRef      styleConfig_;
    osmscout::MapParameter        drawParameter_;
    osmscout::Magnification       magnification_;
    osmscout::GeoCoord            mapCenter_;
    osmscout::GeoCoord            mapCenterJobStart_;
    osmscout::GeoCoord            carPos_;
    osmscout::MapPainterCairo*    painter_;
    osmscout::MercatorProjection  projectionDraw_;
    osmscout::MercatorProjection  projectionSearch_;
    osmscout::AreaSearchParameter searchParameter_;
    osmscout::BreakerRef          breaker_;
    osmscout::MapData             data_;
    std::list<osmscout::TileRef>  mapTiles_;
    SDL_Texture*        markerTexture_;

    double              mapAngle_;
    SDLEventManager*    eventManager_;
    std::thread         mapBackgroundWorker_;
    unsigned char*      mapPixels_;
    SDL_Texture*        mapTexture_;
    SDL_Renderer*       renderer_;

    Uint32              mapEvent_;
    SDL_mutex*          mapMemLock_;
    SDL_sem*            mapLock_;
    SDL_atomic_t        done;

    int                 width_;
    int                 height_;
    int                 mapWidth_;
    int                 mapHeight_;
    SDL_Point           startPoint_;
    SDL_Point           viewDeltaNow_;
    SDL_Point           viewDeltaAtJob_;
    SDL_Point           markerImageSize_;
    bool                renderJobRun_;
    float               screenDpi_;
    
    cairo_surface_t*    image_data_source_;
    cairo_t*            cairoImage_;
    cairo_surface_t*    image_data_marker_;
    bool                paintMarker_;
    double              currentSpeed_;
	double              speedChange_;
    double              carAngle_;
    std::chrono::time_point<std::chrono::high_resolution_clock> lastUpdate_;
    std::chrono::time_point<std::chrono::high_resolution_clock> lastUpdateJobStart_;

	std::ofstream _streamGpxFile;
	std::ofstream _streamGpxFileThink;
	std::list<Step> _positionsThink;

    int RendererThreadMain();
    void RenderMap();
    void RedrawEvent() const;
    void BreakEvent() const;
    void HandleMapEvent(const SDL_Event& event);
    void HandleMouseEvent(const SDL_Event& event);
    void LoadMapData();
    void ClearUnusedData();
public:
	MapPainter(SDLEventManager* eventManager, SDL_Renderer* renderer, float screenDpi);
    ~MapPainter();
		
    Uint32 Init(int width, int height, ConfigManager* config);
    void Draw();
    void HandleEvent(const SDL_Event& event);
    void Resize(int width, int height);
    void CenterMap(const osmscout::GeoCoord& coord, const double& compass, const double& speed);
    void DoUpdateIdle();
	void CloseFiles();
};


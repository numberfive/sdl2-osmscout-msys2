
message(STATUS "working on SDL2-OSMScout-MapPainter dir")

message(STATUS "Compiler ${CMAKE_CXX_COMPILER_ID}")
message(STATUS "Building on ${CMAKE_SYSTEM_NAME}")

FIND_PACKAGE(Cairo REQUIRED)
FIND_PACKAGE(PANGO REQUIRED)

if(NOT SDL2_FOUND)
    if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
        message(FATAL_ERROR "we need sdl2 ENV VAR SDL2DIR can help remeber wenn is windows search vor .lib file")
    endif()
endif()

#if(NOT OSMSCOUT_FOUND)
#    if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
#        message(FATAL_ERROR "we need OSMScout ENV VAR OSMSCOUT_PATH can help remeber wenn is windows search vor .lib file")
#    endif()
#endif()

INCLUDE_DIRECTORIES(${SDL2_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${OSMSCOUT_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${CAIRO_INCLUDE_DIRS})
INCLUDE_DIRECTORIES(${PANGO_INCLUDE_DIRS})

## Use all the *.cpp files we found under this folder for the project
FILE (GLOB_RECURSE MAPPAINTER_SRCS "*.cpp" "*.cxx" "*.cc")

set(project_SRCS ${common_SRCS} ${MAPPAINTER_SRCS})

## Define the executable
add_executable(${PROJECT_NAME} ${project_SRCS})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 11)
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD_REQUIRED ON)

TARGET_LINK_LIBRARIES(${PROJECT_NAME} PRIVATE ${OSMSCOUT_LIBRARIES} ${CAIRO_LIBRARIES})

IF(VCPKG)
	TARGET_LINK_LIBRARIES(${PROJECT_NAME} PRIVATE SDL2::SDL2 SDL2::SDL2main ${SDL2_IMAGE_LIBRARY} ${SDL2_NET_LIBRARY})
ELSE()
	TARGET_LINK_LIBRARIES(${PROJECT_NAME} PRIVATE ${SDL2_LIBRARIES} ${SDL2_IMAGE_LIBRARY} ${SDL2_NET_LIBRARIES})
ENDIF()

MESSAGE(STATUS "Compiler Flags are ${CMAKE_CXX_FLAGS}")

message(STATUS "generate copy resources rules")

set(CURRENTFILE "${MAINFOLDER}/src/logger.conf")
add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                    COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                       "${CURRENTFILE}"      # <--this is in-file
                       $<TARGET_FILE_DIR:${PROJECT_NAME}>)
                       
if(${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
	FOREACH(scoutlib ${OSMSCOUT_LIBRARIES})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for OSMSCOUT Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${CAIRO_LIBRARIES})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for CAIRO Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${ZLIB_LIBRARY})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" "1.dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for ZLIP Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${PNG_LIBRARY})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for PNG Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${PANGO_LIBRARY})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".0.lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for PANGO Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${PANGOCAIRO_LIBRARIES})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".0.lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for PANGO Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${FONTCONFIG_LIBRARY})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for FONTCONFIG Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
	FOREACH(scoutlib ${FREETYPE_LIBRARY})
        if(NOT scoutlib STREQUAL "debug" AND NOT scoutlib STREQUAL "optimized")
            # get_filename_component(DLLSOURCE ${scoutlib} PATH)
            string(REPLACE "/lib/" "/bin/" DLLSOURCE ${scoutlib})
			if(MINGW)
				string(REPLACE ".a" "" DLLSOURCE ${DLLSOURCE})
			else()
				string(REPLACE ".lib" ".dll" DLLSOURCE ${DLLSOURCE})
			endif()
            message(STATUS "Generating Copy Rule for FREETYPE Library ${DLLSOURCE}")   
            add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD # Adds a post-build event to MyTest
                        COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
                            "${DLLSOURCE}"      # <--this is in-file
                            $<TARGET_FILE_DIR:${PROJECT_NAME}>)
        endif()
    ENDFOREACH(scoutlib)
endif()
#pragma once

#include <SDL2/SDL.h>

class SDLEventManager
{
    SDL_mutex* eventLock_;
    SDL_cond* eventWait_;

public:
    SDLEventManager();
    ~SDLEventManager();

    bool Init();
    static Uint32 GetNewEventType();
    bool PushEvent(Uint32 type, Uint32 windowID, Sint32 code, void* data1, void* data2) const;
    int WaitEvent(SDL_Event* event, Uint32 timeout) const;
};


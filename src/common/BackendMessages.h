#pragma once

#include <string>
#include "json.hpp"

#define BACKENDPROTOCOLVER "0.1"
#define SERVERPORT 5022

using json = nlohmann::json;

struct BaseMessage {
    std::string Ver;
    std::string MessageType;
};

struct WelcomeMessage : BaseMessage {
    WelcomeMessage(){
        MessageType = "WelcomeMessage";
        Ver = BACKENDPROTOCOLVER;
    }
    std::string SystemName;
};

struct GPSMessage : BaseMessage {
    GPSMessage(){
        MessageType = "GPSMessage";
        Ver = BACKENDPROTOCOLVER;
        Longitude = 0;
        Latitude = 0;
        Compass = -1;
        Speed = -1;
    }
    double Longitude;
    double Latitude;
    double Compass;
    double Speed;
};

struct ShutdownMessage : BaseMessage {
    ShutdownMessage(){
        MessageType = "ShutdownMessage";
        Ver = BACKENDPROTOCOLVER;
    }
};

void from_json(const json& j, BaseMessage& p);
void to_json(json& j, const WelcomeMessage& p);

void from_json(const json& j, GPSMessage& p);
void to_json(json& j, const GPSMessage& p);

void from_json(const json& j, ShutdownMessage& p);
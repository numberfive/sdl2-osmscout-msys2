#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "ClientSocketConnection"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "../common/easylogging++.h"
#include "../common/BackendMessages.h"
#include "ClientSocketConnection.h"
#include <iostream>

ClientSocketConnection::ClientSocketConnection( const TCPsocket& socket):
    ip_("") {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    socket_ = socket;
    isConnected_ = true;
    currentReadPos_ = 0;
    port_ = 0;
}

ClientSocketConnection::ClientSocketConnection(const std::string &ip, int32_t port) {
    el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
    ip_ = ip;
    port_ = port;
    isConnected_ = false;
    currentReadPos_ = 0;
    socket_ = nullptr;
}

ClientSocketConnection::~ClientSocketConnection() {
    LOG(DEBUG) << "Kill Client Connection";
    
    if(socket_ != nullptr) SDLNet_TCP_Close(socket_);
}

bool ClientSocketConnection::SetupIPAddress()
{
    LOG(DEBUG) << "[CLIENT] Resolving host " << ip_ << " : " << port_;

    // Set up the IPAddress struct so that it has the correct IP and port to the server
    // As stated before, this ONLU sets up the IPaddress. It doesn't try to connect to a server
    int success = SDLNet_ResolveHost( &ipAddress_, ip_.c_str(), port_ );

    if ( success == -1 ) {
         LOG(DEBUG) << "Failed to resolve host " << ip_ << " : " << port_;
        return false;
    }

    LOG(DEBUG) << "Host resolved";
    return true;
}

void ClientSocketConnection::AddToSocketSet(SDLNet_SocketSet* socketSet) {
    SDLNet_TCP_AddSocket( *socketSet, socket_);
}

bool ClientSocketConnection::CheckSocket(bool* needread) {
     *needread = false;
     if(!isConnected_) return false;
    
    int i = SDLNet_SocketReady(socket_);
    if(i > 0) {
        *needread = true;
    }
    if(i < 0) {
        //Error
        return false;
    }
    return true;
}

void  ClientSocketConnection::PharseNewMessage(std::string message) {
    try {
        auto baseMessage = json::parse(message);
        BaseMessage messageJson = baseMessage;
        if(messageJson.Ver == BACKENDPROTOCOLVER) {
            if(callbackFunction_) {
                callbackFunction_(messageJson.MessageType, baseMessage);
            }
        } else {
            LOG(ERROR) << "Backend und Software passen nicht zu sammen";
        }
    }
    catch(json::parse_error& exp) {
        LOG(ERROR) << exp.what();
    } catch(json::out_of_range& exp) {
        LOG(ERROR) << exp.what();
    } catch(json::type_error& exp) {
        LOG(ERROR) << exp.what();
    } catch(std::domain_error& exp) {
        LOG(ERROR) << exp.what();
    } catch(std::runtime_error& exp) {
        LOG(ERROR) << exp.what();
    } catch(std::exception& exp) {
        LOG(ERROR) << exp.what();
    } 
}

void ClientSocketConnection::CheckReadBuffer(int addByteCount) {
    currentReadPos_ += addByteCount;
    int lastBegin = -1;
    int lastEnd = -1;
    for(auto pos = 0; pos < currentReadPos_; pos++) {
        if(readBuffer_[pos] == 0x02) { //STX Found
            lastBegin = pos;
            lastEnd = -1;
        }
        if(readBuffer_[pos] == 0x03) { //ETX Found
            lastEnd = pos;
        }
        if(lastBegin > -1 && lastEnd > -1) {
            auto message = new char[(lastEnd - lastBegin)] ;
            message[(lastEnd - lastBegin) -1] = 0;
            memcpy(message, &readBuffer_[lastBegin + 1], (lastEnd - lastBegin) - 1);
            std::string received(message);
            if(received.size() > 0){
                LOG(DEBUG) << "Received : <" << received << ">";
                PharseNewMessage(received);
            }
            delete [] message;
            lastBegin = -1;
        }
    }
    
    if((currentReadPos_ -1) == lastEnd){
        currentReadPos_ = 0;
        memset(readBuffer_, 0, MAXTCPBUFFERSIZE);
    } else {
        if(lastBegin > -1 && lastEnd == -1){
            memcpy(&readBuffer_[0], &readBuffer_[lastBegin], currentReadPos_ - lastBegin);
            currentReadPos_ = currentReadPos_ - lastBegin;
        } else {
            //becarfull this happend also when Message ist bigger than MAXTCPBUFFERSIZE
            LOG(WARNING) << "schrott kill tcp buffer";
            currentReadPos_ = 0;
            memset(readBuffer_, 0, MAXTCPBUFFERSIZE);
        }
    }
}

void ClientSocketConnection::Read() {
    if(MAXTCPBUFFERSIZE - currentReadPos_ == 0){
        LOG(ERROR) << "ReadBuffer buffer überlauf";
    }
    
    int byteCount = SDLNet_TCP_Recv(socket_, &readBuffer_[currentReadPos_], MAXTCPBUFFERSIZE - currentReadPos_);
   
    LOG(INFO) << "Received : " << byteCount << " bytes";
    
    if(byteCount > 0){
        CheckReadBuffer(byteCount);
    } else if(byteCount == 0){
        isConnected_ = false;
    }
    else if(byteCount < 0){
        isConnected_ = false;
        LOG(ERROR) << SDLNet_GetError();
    }
}

bool ClientSocketConnection::IsConnected() const {
    return isConnected_;
}

void ClientSocketConnection::SendWelcomeMessage() {
    WelcomeMessage message;
    message.SystemName = "Simulator";
    
    json jMessage = message;
    
    Send(jMessage.dump());
}

void ClientSocketConnection::Send(const std::string& str ) {
    int bufferSize = str.length() + 2;
    auto buffer = new char[bufferSize];
    memcpy(&buffer[1], str.c_str(), str.length());
    buffer[0] = 0x02;
    buffer[str.length() + 1] = 0x03;
    
    int bytesSent = SDLNet_TCP_Send(socket_, (const char*)buffer, bufferSize);
    
    delete [] buffer;
    
}

void ClientSocketConnection::Send(json const& message) {
     Send(message.dump());
}

void ClientSocketConnection::SetNewDataCallback(NewDataCallbackFunction callbackFunction) {
    callbackFunction_ = callbackFunction;
}

bool ClientSocketConnection::OpenConnectionToServer() {
    LOG(DEBUG) << "Opening connection Host name : " << ip_ << " : " << port_;

    if(!SetupIPAddress()) return false;
    
    // Try to open the connection to the server. This means the server has to accept the connection
    // ( Which we do using SDLNet_TCP_Accept)
    socket_ = SDLNet_TCP_Open( &ipAddress_ );

    if ( socket_ == nullptr ) {
        LOG(ERROR) << "Failed to open port " << ip_ << " : " << port_ << " Error : " << SDLNet_GetError();
        return false;
    }

    isConnected_ = true;
    LOG(INFO) << "Success!";

    return true;
}

void ClientSocketConnection::CloseSocket() {
     if(socket_ != nullptr) SDLNet_TCP_Close(socket_);
     socket_ = nullptr;
     isConnected_ = false;
}


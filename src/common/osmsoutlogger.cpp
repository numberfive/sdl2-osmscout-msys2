#ifndef ELPP_DEFAULT_LOGGER
#   define ELPP_DEFAULT_LOGGER "OsmSoutLogger"
#endif
#ifndef ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID
#   define ELPP_CURR_FILE_PERFORMANCE_LOGGER_ID ELPP_DEFAULT_LOGGER
#endif

#include "osmsoutlogger.h"
#include "easylogging++.h"
#include <osmscout/util/Logger.h>


namespace utils {
    
Osmsoutlogger::Osmsoutlogger(){
     el::Loggers::getLogger(ELPP_DEFAULT_LOGGER);
     debugDestination_.SetLevel(osmscout::Logger::DEBUG);
     warnDestination_.SetLevel(osmscout::Logger::WARN);
     infoDestination_.SetLevel(osmscout::Logger::INFO);
     errorDestination_.SetLevel(osmscout::Logger::ERROR);
}

Osmsoutlogger::~Osmsoutlogger() = default;

	osmscout::Logger::Line Osmsoutlogger::Log(Level level){
    if (level == osmscout::Logger::DEBUG) {
        return Line(debugDestination_);
    }
    else if (level == osmscout::Logger::WARN) {
      return Line(warnDestination_);
    }
    else if (level == osmscout::Logger::INFO) {
      return Line(infoDestination_);
    }
    else {
      return Line(errorDestination_);
    }
}

StringDestination::StringDestination(): _level(osmscout::Logger::DEBUG) {
		_line = "";
	}

	void StringDestination::Print(const std::string& value){
    _line += value;
}

void StringDestination::Print(const char* value){
    _line += value;
}

void StringDestination::Print(const bool value){
    _line += (value ? "true" : "false");
}

void StringDestination::Print(const short value){
    _line += std::to_string(value);
}

void StringDestination::Print(const unsigned short value){
    _line += std::to_string(value);
}

void StringDestination::Print(int value){
    _line += std::to_string(value);
}

void StringDestination::Print(const unsigned int value){
    _line += std::to_string(value);
}

void StringDestination::Print(long value){
    _line += std::to_string(value);
}

void StringDestination::Print(const unsigned long value){
    _line += std::to_string(value);
}

void StringDestination::Print(const long long value){
    _line += std::to_string(value);
}

void StringDestination::Print(const unsigned long long value){
    _line += std::to_string(value);
}

void StringDestination::PrintLn(){
    if(_level == osmscout::Logger::DEBUG){
        LOG(DEBUG) << _line;
    }
    else if(_level == osmscout::Logger::INFO){
        LOG(INFO) << _line;
    }
    else if(_level == osmscout::Logger::WARN){
        LOG(WARNING) << _line;
    }
    else{
        LOG(ERROR) << _line;
    }
    _line = "";
}

void StringDestination::SetLevel(const osmscout::Logger::Level level){
    _level = level;
}

}//namespace utils

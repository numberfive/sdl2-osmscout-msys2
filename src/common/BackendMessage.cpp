#include "BackendMessages.h"

void from_json(const json& j, BaseMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}

void to_json(json& j, const WelcomeMessage& p) {
     j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"SystemName", p.SystemName}
    };
}

void from_json(const json& j, GPSMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    p.MessageType = j.at("MessageType").get<std::string>();
    p.Longitude = j.at("Longitude").get<double>();
    p.Latitude = j.at("Latitude").get<double>();
    p.Compass = j.at("Compass").get<double>();
    p.Speed = j.at("Speed").get<double>();
}

void to_json(json& j, const GPSMessage& p) {
    j = json {
        {"Ver", p.Ver},
        {"MessageType", p.MessageType},
        {"Latitude", p.Latitude},
        {"Longitude", p.Longitude},
        {"Compass", p.Compass},
        {"Speed", p.Speed}
    };
}

void from_json(const json& j, ShutdownMessage& p) {
    p.Ver = j.at("Ver").get<std::string>();
    auto it_value = j.find("MessageType");
    if(it_value != j.end())
        p.MessageType = j.at("MessageType").get<std::string>();
}


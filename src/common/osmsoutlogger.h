#pragma once
#include <osmscout/util/Logger.h>

namespace utils
{
    class StringDestination : public osmscout::Logger::Destination
    {
			std::string _line;
            osmscout::Logger::Level _level;
        public:
            void Print(const std::string& value) override;
            void Print(const char* value) override;
            void Print(bool value) override;
            void Print(short value) override;
            void Print(unsigned short value) override;
            void Print(int value) override;
            void Print(unsigned int value) override;
            void Print(long value) override;
            void Print(unsigned long value) override;
            void Print(long long value) override;
            void Print(unsigned long long value) override;
            void PrintLn() override;
            StringDestination();
            void SetLevel(osmscout::Logger::Level level);
    };

    class Osmsoutlogger : public osmscout::Logger
    {
            StringDestination debugDestination_;
            StringDestination infoDestination_;
            StringDestination warnDestination_;
            StringDestination errorDestination_;
        public:
            Osmsoutlogger();
            ~Osmsoutlogger();
            
            Line Log(osmscout::Logger::Level level) override;
    };
}

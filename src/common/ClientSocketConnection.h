#pragma once

#include <string>
#include <functional>
#if defined(_MSC_VER)
	#include <SDL_net.h>
#else
	#include <SDL2/SDL_net.h>
#endif
#include "json.hpp"

#define MAXTCPBUFFERSIZE 20240
using json = nlohmann::json;

typedef std::function<void(const std::string& MessageName, json const& Message)> NewDataCallbackFunction;

class ClientSocketConnection
{
    std::string ip_;
    int32_t port_;
    IPaddress ipAddress_;
    NewDataCallbackFunction callbackFunction_;
    TCPsocket socket_;
    int currentReadPos_;
    char readBuffer_[MAXTCPBUFFERSIZE];
    bool isConnected_;
    void CheckReadBuffer(int addByteCount);
    void PharseNewMessage(std::string message);
    bool SetupIPAddress();
public:
    ClientSocketConnection(const TCPsocket &conn);
    ClientSocketConnection(const std::string &ip, int32_t port);
    ~ClientSocketConnection();
    
    void AddToSocketSet( SDLNet_SocketSet* socketSet );
    bool CheckSocket(bool* needread);
    void Read();
    bool IsConnected() const;
    void SendWelcomeMessage();
    void Send(const std::string& str);
    void Send(json const& message);
    void SetNewDataCallback(NewDataCallbackFunction callbackFunction);
    bool OpenConnectionToServer();
    void CloseSocket();
};

